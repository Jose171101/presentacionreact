import './App.css'
import ComponenteDos from './components/ComponenteDos'
import ComponenteUno from './components/ComponenteUno'
import ComponenteTres from './components/ComponenteTres'

function App() {
//debe de ir toda la logica de mi aplicacion
  return (
    //METODO PARA HACER COMENTARIOS FUERA DE CODIGO
    
          <div className='container'>
      <h1 className='titulos'>Mi presentacion</h1>
      <ComponenteUno/>,
      <ComponenteDos />,
      <ComponenteTres />
     
    </div>    
    )
}

export default App
